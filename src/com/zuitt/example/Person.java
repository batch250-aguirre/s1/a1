package com.zuitt.example;

import java.util.Scanner;
public class Person {
    public static void main(String[] args){
        Scanner myObj=new Scanner(System.in);
        double firstSubject, secondSubject, thirdSubject;
        String firstName, lastName;


        System.out.println("First name: ");
        firstName = myObj.nextLine();
        System.out.println("Last name: ");
        lastName = myObj.nextLine();
        System.out.println("First Subject Grade: ");
        firstSubject = myObj.nextDouble();
        System.out.println("Second Subject Grade: ");
        secondSubject = myObj.nextDouble();
        System.out.println("Third Subject Grade: ");
        thirdSubject = myObj.nextDouble();
        System.out.println("Good day, " + firstName + " " + lastName +".");
        double average = (firstSubject+secondSubject+thirdSubject)/3;
        System.out.printf("Your grade average is: %.2f\n", average);
    }
}



